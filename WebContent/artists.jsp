<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="<s:property value="cssFile"/>" type="text/css" rel="stylesheet">
<title><s:text name="caption.artists" /> - <s:property value="cat"/> - <s:property value="title"/></title>
</head>

<body>
<div class="header"><s:include value="common/header.jsp"/></div>
<div class="menu"><s:include value="common/menu.jsp"/></div>
<div class="letters"><s:include value="common/letters.jsp"/></div>
<div class="caption"><s:text name="caption.artists" /> - <s:property value="cat"/></div>
<div class="content">


<s:if test="artistList != null && artistList.size != 0">
	<s:iterator value="artistList" status="st">
	
		<s:if test="#st.index % 2 == 0">
			<div class="artist_list_item_1">
		</s:if>
		<s:else>
			<div class="artist_list_item_2">
		</s:else>
			<s:property value="#st.index+1"/>.
			<s:url id="url" action="Artist"><s:param name="artist_id" value="id"/></s:url><s:a href="%{url}"><s:property value="name"/></s:a>
		</div>
		
	</s:iterator>
</s:if>
<s:elseif test="cat != null && cat != ''">
	<s:text name="artists.noArtistsMessage"/>
</s:elseif>
<s:else>
	<s:text name="artists.noCategoryMessage"/>
</s:else>


</div>
<div class="footer"><s:property value="copyright"/></div>
</body>
</html>