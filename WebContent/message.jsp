<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<s:url id="url" action="%{messageRedirectAction}" /><meta http-equiv="Refresh" content="5;URL=<s:text name="%{url}"/>">
<link href="<s:property value="cssFile"/>" type="text/css" rel="stylesheet">
<title><s:property value="title"/></title>
</head>

<body>
<div class="header"><s:include value="common/header.jsp"/></div>
<div class="menu"><s:include value="common/menu.jsp"/></div>
<div class="letters"><s:include value="common/letters.jsp"/></div>
<div class="caption"><s:text name="caption.message" /></div>
<div class="content">

<s:if test="messageType == 'information'">
	<div class="message_information_text">
	Information:<br>
</s:if>
<s:elseif test="messageType == 'warning'">
	<div class="message_warning_text">
	Warning:<br>
</s:elseif>
<s:elseif test="messageType == 'error'">
	<div class="message_error_text">
	Error:<br>
</s:elseif>

	<s:property value="messageText"/>
	</div>

</div>
<div class="footer"><s:property value="copyright"/></div>
</body>
</html>