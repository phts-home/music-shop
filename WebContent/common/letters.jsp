<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="letters_caption"><s:text name="letters.artists" /></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'A'"/></s:url><s:a href="%{url}">A</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'B'"/></s:url><s:a href="%{url}">B</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'C'"/></s:url><s:a href="%{url}">C</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'D'"/></s:url><s:a href="%{url}">D</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'E'"/></s:url><s:a href="%{url}">E</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'F'"/></s:url><s:a href="%{url}">F</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'G'"/></s:url><s:a href="%{url}">G</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'H'"/></s:url><s:a href="%{url}">H</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'I'"/></s:url><s:a href="%{url}">I</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'J'"/></s:url><s:a href="%{url}">J</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'K'"/></s:url><s:a href="%{url}">K</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'L'"/></s:url><s:a href="%{url}">L</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'M'"/></s:url><s:a href="%{url}">M</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'N'"/></s:url><s:a href="%{url}">N</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'O'"/></s:url><s:a href="%{url}">O</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'P'"/></s:url><s:a href="%{url}">P</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'Q'"/></s:url><s:a href="%{url}">Q</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'R'"/></s:url><s:a href="%{url}">R</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'S'"/></s:url><s:a href="%{url}">S</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'T'"/></s:url><s:a href="%{url}">T</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'U'"/></s:url><s:a href="%{url}">U</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'V'"/></s:url><s:a href="%{url}">V</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'W'"/></s:url><s:a href="%{url}">W</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'X'"/></s:url><s:a href="%{url}">X</s:a></div>
<div class="letter"><s:url id="url" action="Artists"><s:param name="cat" value="'Y'"/></s:url><s:a href="%{url}">Y</s:a></div>
<div class="last_letter"><s:url id="url" action="Artists"><s:param name="cat" value="'Z'"/></s:url><s:a href="%{url}">Z</s:a></div>

