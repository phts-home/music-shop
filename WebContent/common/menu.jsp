<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:url id="url" action="Index"></s:url><s:a href="%{url}"><s:text name="menu.home"/></s:a>
|
<s:if test="#session.username == null || #session.username == ''">
	<s:url id="url" action="login"></s:url><s:a href="%{url}"><s:text name="menu.login"/></s:a>
	|
	<s:url id="url" action="create"></s:url><s:a href="%{url}"><s:text name="menu.create"/></s:a>
</s:if>
<s:else>
	<s:url id="url" action="Cart"></s:url><s:a href="%{url}"><s:text name="menu.cart"/> (<s:property value="cartItemCount"/>)</s:a>
	|
	<s:url id="url" action="Profile"></s:url><s:a href="%{url}"><s:text name="menu.profile"/> (<s:property value="#session.username"/>)</s:a>
	|
	<s:text name="menu.cash" /> <s:property value="cash"/><s:property value="currency"/>
	|
	<s:url id="url" action="Logout"></s:url><s:a href="%{url}"><s:text name="menu.logout"/></s:a>
</s:else>

