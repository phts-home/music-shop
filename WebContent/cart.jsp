<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="<s:property value="cssFile"/>" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<s:property value="jsFile"/>"></script>
<title><s:property value="title"/></title>
</head>

<body>
<div class="header"><s:include value="common/header.jsp"/></div>
<div class="menu"><s:include value="common/menu.jsp"/></div>
<div class="letters"><s:include value="common/letters.jsp"/></div>
<div class="caption"><s:text name="caption.cart" /></div>
<div class="content">

<div class="cart_items">
	<s:if test=" (albums != null && albums.size != 0) || (tracks != null && tracks.size != 0) ">
	
		<s:set name="totalPrice" value="0" />
		
		<s:if test="albums != null && albums.size != 0">
			<div class="cart_items_albums">
				<s:set name="lineIndex" value="1" />
				<div class="check_all"><s:checkbox value="false" name="check_all1" onclick="checkAll('check_all1', 'ch_remove_alb');"/></div>
				<s:iterator value="albums">
					
					<s:if test="#lineIndex % 2 == 1">
						<div class="cart_item_1">
					</s:if>
					<s:else>
						<div class="cart_item_2">
					</s:else>
						<s:checkbox cssClass="ch_remove_alb" value="false" name="%{cartitem_id}"/>
						<s:property value="#lineIndex"/>.
					
						<%-- album --%>
						album
						<s:url id="url" action="Artist"><s:param name="artist_id" value="album.artist.id"/></s:url><s:a href="%{url}"><s:property value="album.artist.name" /></s:a>
						-
						<s:url id="url" action="Album"><s:param name="album_id" value="album.id"/></s:url><s:a href="%{url}"><s:property value="album.title"/> (<s:property value="album.releaseDateString"/>)</s:a>
						<s:property value="album.price"/><s:property value="currency"/>
						<s:set name="totalPrice" value="#totalPrice + album.price"/>
						<s:url id="url" action="RemoveFromCart"><s:param name="cartitem_id" value="cartitem_id"/></s:url><s:a href="%{url}"><s:text name="cart.remove"/></s:a>
					</div>
					<s:set name="lineIndex" value="#lineIndex + 1" />
					
				</s:iterator>
			</div>
		</s:if>
		<s:else>
			<s:text name="cart.noAlbumItems"/>
		</s:else>
			
		<s:if test="tracks != null && tracks.size != 0">
			<div class="cart_items_tracks">
				<s:set name="lineIndex" value="1" />
				<div class="check_all"><s:checkbox value="false" name="check_all2" onclick="checkAll('check_all2', 'ch_remove_tr');"/></div>
				<s:iterator value="tracks">
					
					<s:if test="#lineIndex % 2 == 1">
						<div class="cart_item_1">
					</s:if>
					<s:else>
						<div class="cart_item_2">
					</s:else>
						<s:checkbox cssClass="ch_remove_tr" value="false" name="%{cartitem_id}"/>
						<s:property value="#lineIndex"/>.
					
						<%-- track --%>
						track
						<s:url id="url" action="Artist"><s:param name="artist_id" value="artist.id"/></s:url><s:a href="%{url}"><s:property value="artist.name" /></s:a>
						-
						<s:property value="track.title"/>
						(<s:property value="track.formattedLength"/>)
						(from <s:url id="url" action="Album"><s:param name="album_id" value="album.id"/></s:url><s:a href="%{url}"><s:property value="album.title"/> (<s:property value="album.releaseDateString"/>)</s:a>)
						<s:property value="track.price"/><s:property value="currency"/>
						<s:set name="totalPrice" value="#totalPrice + track.price"/>
						<s:url id="url" action="RemoveFromCart"><s:param name="cartitem_id" value="cartitem_id"/></s:url><s:a href="%{url}"><s:text name="cart.remove"/></s:a>
					</div>
					<s:set name="lineIndex" value="#lineIndex + 1" />
					
				</s:iterator>
			</div>
		</s:if>
		<s:else>
			<s:text name="cart.noTrackItems"/>
		</s:else>
		
		<div class="total_price"><s:text name="cart.totalPrice"/> <s:property value="#totalPrice"/><s:property value="currency"/></div>
		<div class="remove_selected_divbutton"><s:a href="javascript:removeSelected();"><s:text name="cart.removeSelected"/></s:a></div>
		<div class="remove_all_divbutton"><s:url id="url" action="RemoveAll"/><s:a href="%{url}"><s:text name="cart.removeAll"/></s:a></div>
		<div class="buy_divbutton"><s:url id="url" action="PrepareBuy"/><s:a href="%{url}"><s:text name="cart.buy"/></s:a></div>
	</s:if>
	<s:else>
		<s:text name="cart.noItems"/>
	</s:else>
</div>


</div>
<div class="footer"><s:property value="copyright"/></div>
</body>

</html>