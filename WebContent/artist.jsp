<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="<s:property value="cssFile"/>" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<s:property value="jsFile"/>"></script>
<title><s:property value="artist.name" /> - <s:property value="title"/></title>
</head>

<body>
<div class="header"><s:include value="common/header.jsp"/></div>
<div class="menu"><s:include value="common/menu.jsp"/></div>
<div class="letters"><s:include value="common/letters.jsp"/></div>
<div class="caption"><s:property value="artist.name" /></div>
<div class="content">


<div class="artist_name">
	<s:property value="artist.name" />
</div>
<div class="artist_pic">
	<img class="artist_pic_img" src="<s:property value="artist.picPath" />">
</div>
<div class="artist_info">
	<s:property value="artist.info" />
</div>
<div class="artist_discography">
	<s:if test="artist.albums != null && artist.albums.size != 0">
		<div class="artist_check_all"><s:checkbox value="false" name="check_all" onclick="checkAll('check_all', 'ch_buy_alb'); sum();"/></div>
		<s:iterator value="artist.albums" status="st">
		
			<s:if test="#st.index % 2 == 0">
				<div class="artist_discography_album_1">
			</s:if>
			<s:else>
				<div class="artist_discography_album_2">
			</s:else>
				<s:checkbox cssClass="ch_buy_alb" value="false" name="%{id}_%{price}" onclick="sum()"/>
				<s:property value="#st.index+1"/>.
				<s:url id="url" action="Album"><s:param name="album_id" value="id"/></s:url><s:a href="%{url}"><s:property value="title"/></s:a>
				(<s:property value="releaseDateString"/>)
				<s:url id="url" action="AddAlbumsToCart"><s:param name="id" value="id"/></s:url><s:a href="%{url}"><s:text name="cart.buyAlbum"/> <s:property value="price"/><s:property value="currency"/></s:a>
			</div>
			
		</s:iterator>
		<div class="selectedPrice"><span id="selected_price">0</span><s:property value="currency"/></div>
		<div class="buy_selected_albums_divbutton"><s:a href="javascript:buySelectedAlbums();"><s:text name="cart.buySelectedAlbums"/></s:a></div>
	</s:if>
	<s:else>
		<s:text name="artist.noAlbumsMessage" />
	</s:else>
</div>


</div>
<div class="footer"><s:property value="copyright"/></div>
</body>

</html>