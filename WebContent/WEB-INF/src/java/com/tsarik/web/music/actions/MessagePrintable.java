package com.tsarik.web.music.actions;


/**
 * 
 * 
 * @author Phil Tsarik
 *
 */
public interface MessagePrintable {
	
	public static final String TYPE_INFORMATION = "information";
	public static final String TYPE_WARNING = "warning";
	public static final String TYPE_ERROR = "error";
	
	public static final String ACTION_INDEX = "Index";
	
	public String getMessageText();
	public String getMessageType();
	public String getMessageRedirectAction();
	
}
