package com.tsarik.web.music.actions;

import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.exception.JDBCConnectionException;

import com.tsarik.web.music.Properties;
import com.tsarik.web.music.db.Users;
import com.tsarik.web.music.db.util.HibernateUtil;


/**
 * 
 * 
 * @author Phil Tsarik
 *
 */
public class LoginAction extends DefaultAction implements MessagePrintable {
	
	private static final long serialVersionUID = 1L;
	
	// input parameters
	private String username;
	private String password;
	
	// output parameters
	private String messageType = TYPE_INFORMATION;
	private String messageText;
	private String messageRedirectAction = ACTION_INDEX;
	
	
	@Override
	public String execute() throws Exception {
		for (int i = 0; i < username.getBytes().length; i++) {
			System.out.println(username.getBytes()[i]);
		}
		String s = new String(username.getBytes(Charset.forName("UTF-8")), "UTF-8");
		System.out.println(s);
		username = s;
		
		if ( username == null || password == null || username.equals("") ) {
			messageType = TYPE_ERROR;
			messageText = Properties.ERROR_WRONGPASSWORD;
			messageRedirectAction = "login";
			return SUCCESS;
		}
		
		try {
			// create session
			Session hibernateSession = HibernateUtil.currentSession();
			Transaction tx= hibernateSession.beginTransaction();
			
			// build user list
			List userDbList = hibernateSession.createCriteria(Users.class)
				.add(Expression.like("name", username))
				.list();
			
			Iterator userDbListIterator = userDbList.iterator();
			if ( ! userDbListIterator.hasNext() ) {
				messageType = TYPE_ERROR;
				messageText = Properties.ERROR_WRONGPASSWORD;
				messageRedirectAction = "login";
				tx.commit();
				HibernateUtil.closeSession();
				return SUCCESS;
			}
			Users usersRec = (Users)userDbListIterator.next();
			String pass = usersRec.getPassword();
			
			if ( password.equals(pass) ) {
				session.put(Properties.SESSION_USERNAME, username);
				session.put(Properties.SESSION_USERID, usersRec.getId());
			} else {
				messageType = TYPE_ERROR;
				messageText = Properties.ERROR_WRONGPASSWORD;
				messageRedirectAction = "login";
				tx.commit();
				HibernateUtil.closeSession();
				return SUCCESS;
			}
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (JDBCConnectionException e) {
			errorNo = 2001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (HibernateException e) {
			errorNo = 3001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (Exception e) {
			errorNo = 1000;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
		}
		
		if (errorNo != 0) {
			return ERROR;
		}
		messageText = getText("message.hello");
		return SUCCESS;
	}
	
	public String logout() {
		session.clear();
		
		messageText = getText("message.bye");
		
		if (errorNo != 0) {
			return ERROR;
		}
		return SUCCESS;
	}
	
	@Override
	public String getMessageText() {
		return messageText;
	}
	
	@Override
	public String getMessageType() {
		return messageType;
	}
	
	@Override
	public String getMessageRedirectAction() {
		return messageRedirectAction;
	}
	
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
