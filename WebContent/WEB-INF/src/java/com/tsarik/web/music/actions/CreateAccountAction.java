package com.tsarik.web.music.actions;

import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.exception.JDBCConnectionException;

import com.tsarik.web.music.Properties;
import com.tsarik.web.music.db.Users;
import com.tsarik.web.music.db.util.HibernateUtil;


/**
 * 
 * 
 * @author Phil Tsarik
 *
 */
public class CreateAccountAction extends DefaultAction implements MessagePrintable {
	
	private static final long serialVersionUID = 1L;
	
	// input parameters
	private String username;
	private String password;
	private String email;
	
	// output parameters
	private String messageType = TYPE_INFORMATION;
	private String messageText;
	private String messageRedirectAction = ACTION_INDEX;
	
	
	@Override
	public String execute() throws Exception {
		// TODO: кодировки
		/*System.out.println(username);
		for (int i = 0; i < username.getBytes().length; i++) {
			System.out.println(username.getBytes()[i]);
		}
		//String s = new String(username.getBytes("utf-8"), "utf-8");
		//String s = new String(username.getBytes(Charset.forName("UTF-8")));
		String s2 = new String(password.getBytes(Charset.forName("UTF-16")));
		String s3 = new String(email.getBytes(Charset.forName("UTF-8")), "UTF-16");
		//System.out.println(s);
		System.out.println(s2);
		System.out.println(s3);
		//username = s;
		password = s2;
		email = s3;*/
		
		if ( username == null || password == null || email == null || username.equals("") || email.equals("") ) {
			messageType = TYPE_ERROR;
			messageText = Properties.ERROR_DATA;
			messageRedirectAction = "Create";
			return SUCCESS;
		}
		
		try {
			// create session
			Session hibernateSession = HibernateUtil.currentSession();
			Transaction tx= hibernateSession.beginTransaction();
			
			// build user list by name
			List userDbList = hibernateSession.createCriteria(Users.class)
				.add(Expression.like("name", username))
				.list();
			
			// if user with the same name exists
			Iterator userDbListIterator = userDbList.iterator();
			if ( userDbListIterator.hasNext() ) {
				messageType = TYPE_ERROR;
				messageText = Properties.ERROR_USERNAMEEXISTS;
				messageRedirectAction = "Create";
				tx.commit();
				HibernateUtil.closeSession();
				return SUCCESS;
			}
			
			// build user list by email
			List emailDbList = hibernateSession.createCriteria(Users.class)
				.add(Expression.like("email", email))
				.list();
			
			// if user with the same email exists
			Iterator emailDbListIterator = emailDbList.iterator();
			if ( emailDbListIterator.hasNext() ) {
				messageType = TYPE_ERROR;
				messageText = Properties.ERROR_EMAILEXISTS;
				messageRedirectAction = "Create";
				tx.commit();
				HibernateUtil.closeSession();
				return SUCCESS;
			}
			
			// save user in database
			username = new String(username.getBytes(), Charset.forName("UTF-8"));
			Users newUser = new Users(username, password, email, 0.0);
			hibernateSession.save(newUser);
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (JDBCConnectionException e) {
			errorNo = 2001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (HibernateException e) {
			errorNo = 3001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (Exception e) {
			errorNo = 1000;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
		}
		
		if (errorNo != 0) {
			return ERROR;
		}
		messageText = getText("message.created");
		return SUCCESS;
	}
	
	@Override
	public String getMessageText() {
		return messageText;
	}
	
	@Override
	public String getMessageType() {
		return messageType;
	}
	
	@Override
	public String getMessageRedirectAction() {
		return messageRedirectAction;
	}
	
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
		/*System.out.println(username);
		for (int i = 0; i < username.getBytes().length; i++) {
			System.out.println(username.getBytes()[i]);
		}
		this.username = new String(username.getBytes(Charset.forName("UTF-8")));*/
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
