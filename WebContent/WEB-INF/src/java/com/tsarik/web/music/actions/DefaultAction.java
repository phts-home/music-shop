package com.tsarik.web.music.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.exception.JDBCConnectionException;

import com.opensymphony.xwork2.ActionSupport;
import com.tsarik.web.music.Properties;
import com.tsarik.web.music.db.Items;
import com.tsarik.web.music.db.Users;
import com.tsarik.web.music.db.util.HibernateUtil;


/**
 * Base action for the application
 * 
 * @author Phil Tsarik
 *
 */
public class DefaultAction extends ActionSupport implements SessionAware {
	
	private static final long serialVersionUID = 1L;
	
	protected int errorNo = 0;
	protected String errorMessage = "ok";
	
	protected Map<String,Object> session;

	public String execute() throws Exception {
		return SUCCESS;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
	
	public String getTitle() {
		return Properties.SITE_TITLE;
	}
	
	public String getCopyright() {
		return Properties.SITE_COPYRIGHT;
	}
	
	public String getCssFile() {
		return Properties.SITE_CSS_FILE;
	}
	
	public String getJsFile() {
		return Properties.SITE_JS_FILE;
	}
	
	public int getErrorNo() {
		return errorNo;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public String getCurrency() {
		return Properties.CURRENCY_SIGN;
	}
	
	public long getCartItemCount() {
		
		// check whether user logged in or not
		if (session.get(Properties.SESSION_USERNAME) == null) {
			return 0;
		}
		Long userId = (Long)session.get(Properties.SESSION_USERID);
		
		Object co = session.get(Properties.SESSION_CARTITEMSCOUNT);
		if (co != null) {
			Long c = (Long)co;
			return c;
		}
		
		long c = 0;
		try {
			// create session
			Session hibernateSession = HibernateUtil.currentSession();
			Transaction tx = hibernateSession.beginTransaction();
			
			// calculate cart item count
			Query q = hibernateSession.createQuery("SELECT COUNT(items) FROM com.tsarik.web.music.db.Items AS items WHERE items.user_id = :userId");
			q.setParameter("userId", userId, Hibernate.LONG);
			List list = q.list();
			
			c = (Long)list.get(0);
			session.put(Properties.SESSION_CARTITEMSCOUNT, c);
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (JDBCConnectionException e) {
			e.printStackTrace();
			return 0;
			
		} catch (HibernateException e) {
			e.printStackTrace();
			return 0;
			
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		
		return c;
	}
	
	public double getCash() {
		// check whether user logged in or not
		if (session.get(Properties.SESSION_USERNAME) == null) {
			return 0;
		}
		Long userId = (Long)session.get(Properties.SESSION_USERID);
		
		Object co = session.get(Properties.SESSION_CASH);
		if (co != null) {
			double c = (Double)co;
			return c;
		}
		
		double c = 0;
		try {
			// create session
			Session hibernateSession = HibernateUtil.currentSession();
			Transaction tx = hibernateSession.beginTransaction();
			
			// calculate user's cash
			Users usersRec = (Users)hibernateSession.load(Users.class, userId);
			
			c = usersRec.getCash();
			session.put(Properties.SESSION_CASH, c);
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (JDBCConnectionException e) {
			e.printStackTrace();
			return 0;
			
		} catch (HibernateException e) {
			e.printStackTrace();
			return 0;
			
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		
		return c;
	}
	
}
