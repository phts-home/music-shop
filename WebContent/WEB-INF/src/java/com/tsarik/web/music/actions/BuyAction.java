package com.tsarik.web.music.actions;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.exception.JDBCConnectionException;

import com.tsarik.web.music.Properties;
import com.tsarik.web.music.db.AlbumItem;
import com.tsarik.web.music.db.Albums;
import com.tsarik.web.music.db.Items;
import com.tsarik.web.music.db.TrackItem;
import com.tsarik.web.music.db.Tracks;
import com.tsarik.web.music.db.Users;
import com.tsarik.web.music.db.util.HibernateUtil;

public class BuyAction extends DefaultAction implements MessagePrintable {
	
	private static final long serialVersionUID = 1L;
	
	
	// input parameters
	
	// output parameters
	private String messageType = TYPE_INFORMATION;
	private String messageText;
	private String messageRedirectAction = ACTION_INDEX;
	
	
	@Override
	public String execute() throws Exception {
		// check whether user logged in or not
		if (session.get(Properties.SESSION_USERNAME) == null) {
			return INPUT;
		}
		Long userId = (Long)session.get(Properties.SESSION_USERID);
		
		try {
			// create session
			Session hibernateSession = HibernateUtil.currentSession();
			Transaction tx= hibernateSession.beginTransaction();
			
			// load user's record from database
			Users usersRec = (Users)hibernateSession.get(Users.class, userId);
			if (usersRec == null) {
				errorNo = 4001;
				errorMessage = Properties.ERROR_USERNOTFOUND;
				return ERROR;
			}
			double cash = usersRec.getCash();
			
			// build cart item list
			List itemDbList = hibernateSession.createCriteria(Items.class)
				.add(Expression.like("user_id", userId))
				.list();
			
			double totalPrice = 0;
			Iterator itemDbListIterator = itemDbList.iterator();
			while (itemDbListIterator.hasNext()) {
				Items itemsRec = (Items)itemDbListIterator.next();
				
				Long itemId = itemsRec.getItem_id();
				
				//if (itemsRec.getItem_type().equalsIgnoreCase("album")) {
				if (itemsRec instanceof AlbumItem) {
					// build album information
					try {
						Albums albumsRec = (Albums)hibernateSession.get(Albums.class, itemId);
						if (albumsRec == null) {
							continue;
						}
						totalPrice += albumsRec.getPrice();
					} catch (HibernateException e) {
						// TODO: ??
						continue;
					}
				//} else if (itemsRec.getItem_type().equalsIgnoreCase("track")) {
				} else if (itemsRec instanceof TrackItem) {
					// build track information
					try {
						Tracks tracksRec = (Tracks)hibernateSession.get(Tracks.class, itemId);
						if (tracksRec == null) {
							continue;
						}
						totalPrice += tracksRec.getPrice();
					} catch (HibernateException e) {
						// TODO: ??
						continue;
					}
				}
			}
			
			// check balance
			if (cash < totalPrice) {
				tx.commit();
				HibernateUtil.closeSession();
				messageType = TYPE_ERROR;
				messageText = getText("message.notEnough");
				messageRedirectAction = "Cart";
				return SUCCESS;
			}
			
			// take money from user
			usersRec.setCash(usersRec.getCash() - totalPrice);
			double newCash = usersRec.getCash();
			
			// save cash value to session's parameter
			session.put(Properties.SESSION_CASH, newCash);
			
			// clear shopping cart
			for (int i = 0; i < itemDbList.size(); i++) {
				hibernateSession.delete(itemDbList.get(i));
			}
			session.put(Properties.SESSION_CARTITEMSCOUNT, new Long(0));
			
			// update database
			hibernateSession.update(usersRec);
			
			// set output message
			messageText = getText("message.purchaseSuccess");
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (JDBCConnectionException e) {
			errorNo = 2001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (HibernateException e) {
			errorNo = 3001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (Exception e) {
			errorNo = 1000;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
		}
		
		if (errorNo != 0) {
			return ERROR;
		}
		
		
		return SUCCESS;
	}
	
	public String prepare() {
		// check whether user logged in or not
		if (session.get(Properties.SESSION_USERNAME) == null) {
			return INPUT;
		}
		Long userId = (Long)session.get(Properties.SESSION_USERID);
		
		try {
			// create session
			Session hibernateSession = HibernateUtil.currentSession();
			Transaction tx= hibernateSession.beginTransaction();
			
			
			
			
			
			
			
			
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (JDBCConnectionException e) {
			errorNo = 2001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (HibernateException e) {
			errorNo = 3001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (Exception e) {
			errorNo = 1000;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
		}
		
		if (errorNo != 0) {
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	@Override
	public String getMessageType() {
		return messageType;
	}
	
	@Override
	public String getMessageText() {
		return messageText;
	}
	
	@Override
	public String getMessageRedirectAction() {
		return messageRedirectAction;
	}
	
	
	
}
