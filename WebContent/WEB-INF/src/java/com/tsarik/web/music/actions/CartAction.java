package com.tsarik.web.music.actions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.exception.JDBCConnectionException;

import com.tsarik.web.music.Properties;
import com.tsarik.web.music.db.AlbumItem;
import com.tsarik.web.music.db.Albums;
import com.tsarik.web.music.db.Artists;
import com.tsarik.web.music.db.Items;
import com.tsarik.web.music.db.TrackItem;
import com.tsarik.web.music.db.Tracks;
import com.tsarik.web.music.db.util.HibernateUtil;


/**
 * 
 * 
 * @author Phil Tsarik
 *
 */
public class CartAction extends DefaultAction {
	
	private static final long serialVersionUID = 1L;
	
	// input parameters
	private String id;
	private String cartitem_id;
	
	// output parameters
	private List<AlbumItem> albums;
	private List<TrackItem> tracks;
	
	
	@Override
	public String execute() throws Exception {
		// check whether user logged in or not
		if (session.get(Properties.SESSION_USERNAME) == null) {
			return INPUT;
		}
		Long userId = (Long)session.get(Properties.SESSION_USERID);
		
		// create and clear output lists
		if (albums == null) {
			albums = new ArrayList<AlbumItem>();
		} else {
			albums.clear();
		}
		if (tracks == null) {
			tracks = new ArrayList<TrackItem>();
		} else {
			tracks.clear();
		}
		
		try {
			// create session
			Session hbtSession = HibernateUtil.currentSession();
			Transaction tx = hbtSession.beginTransaction();
			
			// create item list
			createItemList(hbtSession, userId);
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (JDBCConnectionException e) {
			errorNo = 2001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (HibernateException e) {
			errorNo = 3001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (Exception e) {
			errorNo = 1000;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
		}
		
		if (errorNo != 0) {
			return ERROR;
		}
		return SUCCESS;
	}
	
	public String addAlbumsToCart() {
		// check whether user logged in or not
		if (session.get(Properties.SESSION_USERNAME) == null) {
			return INPUT;
		}
		Long userId = (Long)session.get(Properties.SESSION_USERID);
		System.out.println(id);
		try {
			// create session
			Session hbtSession = HibernateUtil.currentSession();
			Transaction tx = hbtSession.beginTransaction();
			
			// parse ids from string and add all items with this ids
			StringTokenizer st = new StringTokenizer(id, ",");
		    while (st.hasMoreTokens()) {
		    	// parse track id
		    	Long albumId = null;
				try {
					albumId = Long.parseLong(st.nextToken());
				} catch (NumberFormatException e) {
					continue;
				}
				
				// delete all tracks of this album from the cart
				List itemDbList = hbtSession.createCriteria(TrackItem.class)
					.add(Expression.like("user_id", userId))
					//.add(Expression.like("item_type", Items.TYPE_TRACK))
					.list();
				Iterator itemDbListIterator = itemDbList.iterator();
				while (itemDbListIterator.hasNext()) {
					Items itemsRec = (Items)itemDbListIterator.next();
					Tracks trackRec = (Tracks)hbtSession.load(Tracks.class, itemsRec.getItem_id());
					if (albumId.equals(trackRec.getAlbum_id())) {
						hbtSession.delete(itemsRec);
					}
				}
				
				// if this album already added in the cart then return
				List albumDbList = hbtSession.createCriteria(AlbumItem.class)
					.add(Expression.like("user_id", userId))
					.add(Expression.like("item_id", albumId))
					//.add(Expression.like("item_type", Items.TYPE_ALBUM))
					.list();
				Iterator albumDbListIterator = albumDbList.iterator();
				if (albumDbListIterator.hasNext()) {
					continue;
				}
				
				// save cart item to database
				Items newItemsRec = new AlbumItem(userId, albumId/*, Items.TYPE_ALBUM*/);
				hbtSession.save(newItemsRec);
		    }
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (JDBCConnectionException e) {
			errorNo = 2001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (HibernateException e) {
			errorNo = 3001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (Exception e) {
			errorNo = 1000;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
		}
		
		if (errorNo != 0) {
			return ERROR;
		}
		return SUCCESS;
	}
	
	public String addTracksToCart() {
		// check whether user logged in or not
		if (session.get(Properties.SESSION_USERNAME) == null) {
			return INPUT;
		}
		Long userId = (Long)session.get(Properties.SESSION_USERID);
		System.out.println(id);
		try {
			// create session
			Session hibernateSession = HibernateUtil.currentSession();
			Transaction tx = hibernateSession.beginTransaction();
			
			// parse ids from string and add all items with this ids
			StringTokenizer st = new StringTokenizer(id, ",");
		    while (st.hasMoreTokens()) {
		    	// parse track id
		    	Long trackId = null;
				try {
					trackId = Long.parseLong(st.nextToken());
				} catch (NumberFormatException e) {
					continue;
				}
				
				// if this track already added in the cart then return
				List trackDbList = hibernateSession.createCriteria(TrackItem.class)
					.add(Expression.like("user_id", userId))
					.add(Expression.like("item_id", trackId))
					//.add(Expression.like("item_type", Items.TYPE_TRACK))
					.list();
				Iterator trackDbListIterator = trackDbList.iterator();
				if (trackDbListIterator.hasNext()) {
					continue;
				}
				
				// if the album of this track already added in the cart then return
				Tracks trackRec = (Tracks)hibernateSession.load(Tracks.class, trackId);
				List itemDbList = hibernateSession.createCriteria(AlbumItem.class)
					.add(Expression.like("user_id", userId))
					.add(Expression.like("item_id", trackRec.getAlbum_id()))
					//.add(Expression.like("item_type", Items.TYPE_ALBUM))
					.list();
				Iterator itemDbListIterator = itemDbList.iterator();
				if (itemDbListIterator.hasNext()) {
					continue;
				}
				
				// save cart item to database
				Items newItemsRec = new TrackItem(userId, trackId/*, Items.TYPE_TRACK*/);
				hibernateSession.save(newItemsRec);
		    }
		    
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (JDBCConnectionException e) {
			errorNo = 2001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (HibernateException e) {
			errorNo = 3001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (Exception e) {
			errorNo = 1000;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
		}
		
		if (errorNo != 0) {
			return ERROR;
		}
		return SUCCESS;
	}
	
	public String removeFromCart() {
		// check whether user logged in or not
		if (session.get(Properties.SESSION_USERNAME) == null) {
			return INPUT;
		}
		System.out.println(cartitem_id);
		try {
			// create session
			Session hibernateSession = HibernateUtil.currentSession();
			Transaction tx= hibernateSession.beginTransaction();
			
			// parse ids from string and delete all items with this ids
			StringTokenizer st = new StringTokenizer(cartitem_id, ",");
		    while (st.hasMoreTokens()) {
				try {
					Items itemsRec = (Items)hibernateSession.get(Items.class, Long.parseLong(st.nextToken()));
					if (itemsRec == null) {
						continue;
					}
					hibernateSession.delete(itemsRec);
				} catch (NumberFormatException e) {
					continue;
				}
		    }
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (JDBCConnectionException e) {
			errorNo = 2001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (HibernateException e) {
			errorNo = 3001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (Exception e) {
			errorNo = 1000;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
		}
		
		if (errorNo != 0) {
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	public String removeAll() {
		// check whether user logged in or not
		if (session.get(Properties.SESSION_USERNAME) == null) {
			return INPUT;
		}
		Long userId = (Long)session.get(Properties.SESSION_USERID);
		
		try {
			// create session
			Session hibernateSession = HibernateUtil.currentSession();
			Transaction tx = hibernateSession.beginTransaction();
			
			// build cart item list
			List itemDbList = hibernateSession.createCriteria(Items.class)
				.add(Expression.like("user_id", userId))
				.list();
			
			// delete items
			Iterator itemDbListIterator = itemDbList.iterator();
			while (itemDbListIterator.hasNext()) {
				Items itemsRec = (Items)itemDbListIterator.next();
				hibernateSession.delete(itemsRec);
			}
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (JDBCConnectionException e) {
			errorNo = 2001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (HibernateException e) {
			errorNo = 3001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (Exception e) {
			errorNo = 1000;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
		}
		
		if (errorNo != 0) {
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	
	
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setCartitem_id(String cartitem_id) {
		this.cartitem_id = cartitem_id;
	}
	
	/*public List<Item> getItemList() {
		return itemList;
	}*/
	
	public List<AlbumItem> getAlbums() {
		return albums;
	}
	
	public List<TrackItem> getTracks() {
		return tracks;
	}
	
	
	
	
	private void createItemList(Session hibernateSession, Long userId) throws Exception {
		// build cart item list
		List itemDbList = hibernateSession.createCriteria(Items.class)
			.add(Expression.like("user_id", userId))
			.addOrder(Order.asc("cartitem_id"))
			//.addOrder(Order.asc("item_type"))
			.list();
		
		// save item count to session's parameter
		session.put(Properties.SESSION_CARTITEMSCOUNT, Long.valueOf(itemDbList.size()).longValue());
		
		Iterator itemDbListIterator = itemDbList.iterator();
		while (itemDbListIterator.hasNext()) {
			Items itemsRec = (Items)itemDbListIterator.next();
			
			/*Long itemId = itemsRec.getItem_id();
			Long albumId = null;
			
			Tracks tracksRec = null;*/
			if (itemsRec instanceof AlbumItem) {
				AlbumItem albumItem = (AlbumItem)itemsRec;
				
				// build album information
				Albums albumsRec = null;
				try {
					albumsRec = (Albums)hibernateSession.get(Albums.class, albumItem.getAlbum_id());
					if (albumsRec == null) {
						continue;
					}
				} catch (HibernateException e) {
					// TODO: ??
					continue;
				}
				
				// build artist information
				Artists artistsRec = null;
				try {
					artistsRec = (Artists)hibernateSession.get(Artists.class, albumsRec.getArtist_id());
					if (artistsRec == null) {
						continue;
					}
				} catch (HibernateException e) {
					// TODO: ??
					continue;
				}
				
				albumsRec.setArtist(artistsRec);
				albumItem.setAlbum(albumsRec);
				
				albums.add(albumItem);
				
			} else if (itemsRec instanceof TrackItem) {
				TrackItem trackItem = (TrackItem)itemsRec;
				
				// build track information
				Tracks tracksRec = null;
				try {
					tracksRec = (Tracks)hibernateSession.get(Tracks.class, trackItem.getTrack_id());
					if (tracksRec == null) {
						continue;
					}
				} catch (HibernateException e) {
					// TODO: ??
					continue;
				}
				
				// build album information
				Albums albumsRec = null;
				try {
					albumsRec = (Albums)hibernateSession.get(Albums.class, tracksRec.getAlbum_id());
					if (albumsRec == null) {
						continue;
					}
				} catch (HibernateException e) {
					// TODO: ??
					continue;
				}
				tracksRec.setAlbum(albumsRec);
				
				// build artist information
				Artists artistsRec = null;
				try {
					artistsRec = (Artists)hibernateSession.get(Artists.class, albumsRec.getArtist_id());
					if (artistsRec == null) {
						continue;
					}
				} catch (HibernateException e) {
					// TODO: ??
					continue;
				}
				albumsRec.setArtist(artistsRec);
				
				
				trackItem.setTrack(tracksRec);
				
				tracks.add(trackItem);
				
			}
			
			
			//Item item = new Item(itemsRec.getCartitem_id(), artistsRec, albumsRec, tracksRec, itemsRec.getItem_type());
			//itemList.add(item);
		}
	}
	
}
