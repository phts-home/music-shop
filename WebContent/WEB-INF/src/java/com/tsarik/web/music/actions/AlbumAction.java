package com.tsarik.web.music.actions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.exception.JDBCConnectionException;

import com.tsarik.web.music.Properties;
import com.tsarik.web.music.db.Albums;
import com.tsarik.web.music.db.Artists;
import com.tsarik.web.music.db.Tracks;
import com.tsarik.web.music.db.util.HibernateUtil;

public class AlbumAction extends DefaultAction {
	
	private static final long serialVersionUID = 1L;
	
	// input parameters
	private String album_id;
	
	// output parameters
	private Albums album;
	
	
	@Override
	public String execute() throws Exception {
		
		try {
			// convert id from string to long value
			Long albumId = Long.valueOf(album_id);
			
			// create session
			Session hibernateSession = HibernateUtil.currentSession();
			Transaction tx= hibernateSession.beginTransaction();
			
			// build album information
			album = (Albums)hibernateSession.get(Albums.class, albumId);
			
			if (album == null) {
				errorNo = 3003;
				errorMessage = Properties.ERROR_ALBUM;
				tx.commit();
				HibernateUtil.closeSession();
				return ERROR;
			}
			
			/*Calendar c = new GregorianCalendar();
			c.setTime(album.getRelease_date().getTime());
			album = new Album(album.getId(), album.getArtist_id(), album.getTitle(), album.getPic(), c, album.getPrice(), null);*/
			
			// build artist information
			Artists artist = (Artists)hibernateSession.get(Artists.class, album.getArtist_id());
			
			if (artist == null) {
				errorNo = 3002;
				errorMessage = Properties.ERROR_ARTIST;
				tx.commit();
				HibernateUtil.closeSession();
				return ERROR;
			}
			
			album.setArtist(artist);
			
			// fetch tracks from database
			album.getTracks().iterator();
			// TODO del
			//Iterator tracksIterator = album.getTracks().iterator();
			/*while (tracksIterator.hasNext()) {
				Tracks rec = (Tracks)tracksIterator.next();
				System.out.println(rec);
			}*/
			
			// build track list
			/*List trackDbList = hibernateSession.createCriteria(Tracks.class)
				.add(Expression.like("album_id", album.getId()))
				.addOrder(Order.asc("pos"))
				.list();
			
			/*ArrayList<Track> trackList = new ArrayList<Track>();
			Iterator trackDbListIterator = trackDbList.iterator();
			while (trackDbListIterator.hasNext()) {
				Tracks tracksRec = (Tracks)trackDbListIterator.next();
				Track tr = new Track(tracksRec.getTrack_id(), tracksRec.getAlbum_id(), tracksRec.getTitle(), tracksRec.getPos(), tracksRec.getLength(), tracksRec.getPrice(), tracksRec.getLyrics());
				trackList.add(tr);
			}*/
			
			//album.setTrackList(trackDbList);
			
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (NumberFormatException e) {
			errorNo = 4001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (JDBCConnectionException e) {
			errorNo = 2001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (HibernateException e) {
			errorNo = 3001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (Exception e) {
			errorNo = 1000;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
		}
		
		if (album == null) {
			errorNo = 1000;
			errorMessage = Properties.ERROR_UNEXPECTED;
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	
	public String getAlbum_id() {
		return album_id;
	}
	
	public void setAlbum_id(String albumId) {
		album_id = albumId;
	}
	
	public Artists getArtist() {
		return album.getArtist();
	}
	
	public Albums getAlbum() {
		return album;
	}
	
}
