package com.tsarik.web.music.actions;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.JDBCConnectionException;

import com.tsarik.web.music.Properties;
import com.tsarik.web.music.db.Tracks;
import com.tsarik.web.music.db.util.HibernateUtil;

public class LyricsAction extends DefaultAction {
	
	private static final long serialVersionUID = 1L;
	
	// input parameters
	private String track_id;
	
	// output parameters
	private String lyrics;
	
	
	
	@Override
	public String execute() throws Exception {
		
		try {
			// convert id from string to long value
			Long trackId = Long.valueOf(track_id);
			
			// create session
			Session hibernateSession = HibernateUtil.currentSession();
			Transaction tx= hibernateSession.beginTransaction();
			
			// build album information
			Tracks tracksRec = (Tracks)hibernateSession.get(Tracks.class, trackId);
			
			if (tracksRec == null) {
				errorNo = 3004;
				errorMessage = Properties.ERROR_TRACK;
				tx.commit();
				HibernateUtil.closeSession();
				return ERROR;
			}
			
			lyrics = tracksRec.getLyrics();
			
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (NumberFormatException e) {
			errorNo = 4001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (JDBCConnectionException e) {
			errorNo = 2001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (HibernateException e) {
			errorNo = 3001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (Exception e) {
			errorNo = 1000;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	
	public void setTrack_id(String trackId) {
		track_id = trackId;
	}
	
	public String getLyrics() {
		return lyrics;
	}
	
	
}
