package com.tsarik.web.music.db;

public class TrackItem extends Items {
	
	public TrackItem() {
		// TODO Auto-generated constructor stub
	}
	
	public TrackItem(Long user_id, Long track_id) {
		super(user_id, track_id);
	}
	
	
	
	public Long getTrack_id() {
		return item_id;
	}
	
	
	
	public Tracks getTrack() {
		return track;
	}
	
	public void setTrack(Tracks track) {
		this.track = track;
	}
	
	
	
	public Albums getAlbum() {
		return track.getAlbum();
	}
	
	public Artists getArtist() {
		return track.getAlbum().getArtist();
	}
	
	
	
	private Tracks track;
	
}
