package com.tsarik.web.music.actions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.exception.JDBCConnectionException;

import com.tsarik.web.music.db.Artists;
import com.tsarik.web.music.db.util.HibernateUtil;

public class ArtistsAction extends DefaultAction {
	
	private static final long serialVersionUID = 1L;
	
	// input parameters
	private String cat;
	
	// output parameters
	private List<Artists> artistDbList;
	
	
	@Override
	public String execute() throws Exception {
		
		// create and clear output list
		/*if (artistList == null) {
			artistList = new ArrayList<Artist>();
		} else {
			artistList.clear();
		}*/
		
		try {
			// create session
			Session hibernateSession = HibernateUtil.currentSession();
			Transaction tx= hibernateSession.beginTransaction();
			
			// build artist list
			artistDbList = hibernateSession.createCriteria(Artists.class)
				.add(Expression.like("category", cat))
				.addOrder(Order.asc("name"))
				.list();
			
			/*Iterator artistDbListIterator = artistDbList.iterator();
			while (artistDbListIterator.hasNext()) {
				Artists rec = (Artists)artistDbListIterator.next();
				Artist artist = new Artist(rec.getArtist_id(), rec.getName(), rec.getInfo(), rec.getPic(), rec.getCategory());
				artistList.add(artist);
			}*/
			
			
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (NumberFormatException e) {
			errorNo = 4001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (JDBCConnectionException e) {
			errorNo = 2001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (HibernateException e) {
			errorNo = 3001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (Exception e) {
			errorNo = 1000;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	
	public String getCat() {
		return cat;
	}
	
	public void setCat(String cat) {
		this.cat = cat;
	}
	
	public List<Artists> getArtistList() {
		return artistDbList;//TODO
	}
	
}
