package com.tsarik.web.music;

public class Properties {
	
	public static final String SITE_TITLE = "Music";
	public static final String SITE_COPYRIGHT = "(c) Phil Tsarik, 2010";
	
	public static final String SITE_CSS_FILE = "/Music/css/style.css";
	public static final String SITE_JS_FILE = "/Music/js/main.js";
	
	public static final String ALBUM_PICS_DIR = "/Music/pics/";
	public static final String ARTIST_PICS_DIR = "/Music/pics/";
	
	public static final String ERROR_UNEXPECTED = "Unexpected error occured";
	public static final String ERROR_WRONGPASSWORD = "Wrong username or password";
	public static final String ERROR_ARTIST = "Unable to load artist information";
	public static final String ERROR_ALBUM = "Unable to load album information";
	public static final String ERROR_TRACK = "Unable to load track information";
	public static final String ERROR_DATA = "Wrong data";
	public static final String ERROR_USERNAMEEXISTS = "This username already exists";
	public static final String ERROR_EMAILEXISTS = "This email already exists";
	public static final String ERROR_USERNOTFOUND = "User not found";
	
	public static final String CURRENCY_SIGN = "$";
	
	public static final String SESSION_USERNAME = "username";
	public static final String SESSION_USERID = "user_id";
	public static final String SESSION_CARTITEMSCOUNT = "cartitem_count";
	public static final String SESSION_CASH = "user_cash";
	
}
