package com.tsarik.web.music.db;

public class AlbumItem extends Items {
	
	public AlbumItem() {
		// TODO Auto-generated constructor stub
	}
	
	public AlbumItem(Long user_id, Long album_id) {
		super(user_id, album_id);
	}
	
	
	
	public Long getAlbum_id() {
		return item_id;
	}
	
	
	
	public Albums getAlbum() {
		return album;
	}
	
	public void setAlbum(Albums album) {
		this.album = album;
	}
	
	
	
	
	private Albums album;
	
}
