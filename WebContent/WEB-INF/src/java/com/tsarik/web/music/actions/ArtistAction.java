package com.tsarik.web.music.actions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.exception.JDBCConnectionException;

import com.tsarik.web.music.Properties;
import com.tsarik.web.music.db.Albums;
import com.tsarik.web.music.db.Artists;
import com.tsarik.web.music.db.util.HibernateUtil;

public class ArtistAction extends DefaultAction {
	
	private static final long serialVersionUID = 1L;
	
	// input parameters
	private String artist_id;
	
	// output parameters
	private Artists artist;
	
	
	@Override
	public String execute() throws Exception {
		
		try {
			// convert id from string to long value
			Long artistId = Long.valueOf(artist_id);
			
			// create session
			Session hibernateSession = HibernateUtil.currentSession();
			Transaction tx= hibernateSession.beginTransaction();
			
			// build artist information
			artist = (Artists)hibernateSession.get(Artists.class, artistId);
			
			if (artist == null) {
				errorNo = 3002;
				errorMessage = Properties.ERROR_ARTIST;
				tx.commit();
				HibernateUtil.closeSession();
				return ERROR;
			}
			
			// fetch albums from database
			artist.getAlbums().iterator();
			
			// TODO del
			/*Iterator albumsIterator = artist.getAlbums().iterator();
			while (albumsIterator.hasNext()) {
				Albums rec = (Albums)albumsIterator.next();
				System.out.println(rec);
			}*/
			
			
			// build album list
			/*List albumDbList = hibernateSession.createCriteria(Albums.class)
				.add(Expression.like("artist_id", artistId))
				.addOrder(Order.asc("release_date"))
				.list();
			
			/*ArrayList<Album> albumList = new ArrayList<Album>();
			Iterator albumDbListIterator = albumDbList.iterator();
			while (albumDbListIterator.hasNext()) {
				Albums albumsRec = (Albums)albumDbListIterator.next();
				Calendar c = new GregorianCalendar();
				c.setTime(albumsRec.getRelease_date().getTime());
				Album alb = new Album(albumsRec.getId(), albumsRec.getArtist_id(), albumsRec.getTitle(), albumsRec.getPic(), c, albumsRec.getPrice(), null);
				albumList.add(alb);
			}*/
			//artist.setAlbumList(albumDbList);
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (NumberFormatException e) {
			errorNo = 4001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (JDBCConnectionException e) {
			errorNo = 2001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (HibernateException e) {
			errorNo = 3001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (Exception e) {
			errorNo = 1000;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
		}
		
		if (artist == null) {
			errorNo = 1000;
			errorMessage = Properties.ERROR_UNEXPECTED;
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	
	public String getArtist_id() {
		return artist_id;
	}
	
	public void setArtist_id(String artistId) {
		artist_id = artistId;
	}
	
	public Artists getArtist() {
		return artist;
	}
	
}
