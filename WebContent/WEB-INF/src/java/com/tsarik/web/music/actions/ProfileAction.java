package com.tsarik.web.music.actions;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.JDBCConnectionException;

import com.tsarik.web.music.Properties;
import com.tsarik.web.music.db.Users;
import com.tsarik.web.music.db.util.HibernateUtil;



public class ProfileAction extends DefaultAction {
	
	private static final long serialVersionUID = 1L;
	
	// input parameters
	
	// output parameters
	Users user = null;
	

	@Override
	public String execute() throws Exception {
		// check whether user logged in or not
		if (session.get(Properties.SESSION_USERNAME) == null) {
			return INPUT;
		}
		Long userId = (Long)session.get(Properties.SESSION_USERID);
		
		try {
			// create session
			Session hibernateSession = HibernateUtil.currentSession();
			Transaction tx= hibernateSession.beginTransaction();
			
			// load user's record from database
			user = (Users)hibernateSession.get(Users.class, userId);
			if (user == null) {
				errorNo = 4001;
				errorMessage = Properties.ERROR_USERNOTFOUND;
				return ERROR;
			}
			
			/*user = new User(usersRec.getName(), usersRec.getPassword(), usersRec.getEmail(), usersRec.getCash());
			session.put(Properties.SESSION_CASH, usersRec.getCash());*/
			
			// close session
			tx.commit();
			HibernateUtil.closeSession();
			
		} catch (JDBCConnectionException e) {
			errorNo = 2001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (HibernateException e) {
			errorNo = 3001;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
			
		} catch (Exception e) {
			errorNo = 1000;
			errorMessage = e.getMessage();
			e.printStackTrace();
			return ERROR;
		}
		
		if (errorNo != 0) {
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	
	
	public Users getUser() {
		return user;
	}
	
	
}
