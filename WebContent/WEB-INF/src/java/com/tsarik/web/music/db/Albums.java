package com.tsarik.web.music.db;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.tsarik.web.music.Properties;


/**
 * 
 * 
 * @author Phil Tsarik
 *
 */
public class Albums {
	
	public Albums() {
		// TODO Auto-generated constructor stub
	}
	
	public Albums(Long id, Long artistId, String title, String pic,  Calendar releaseDate, double price, Set tracks) {
		this.id = id;
		this.artist_id = artistId;
		this.title = title;
		this.pic = pic;
		this.release_date = releaseDate;
		this.tracks = tracks;
		this.price = price;
	}
	
	
	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getArtist_id() {
		return artist_id;
	}
	
	public void setArtist_id(Long artistId) {
		artist_id = artistId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getPic() {
		return pic;
	}
	
	public void setPic(String pic) {
		this.pic = pic;
	}
	
	public Calendar getRelease_date() {
		return release_date;
	}
	
	public void setRelease_date(Calendar releaseDate) {
		release_date = releaseDate;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public Set<Tracks> getTracks() {
		return tracks;
	}
	
	public void setTracks(Set tracks) {
		this.tracks = tracks;
	}
	
	public Artists getArtist() {
		return artist;
	}
	
	public void setArtist(Artists artist) {
		this.artist = artist;
	}
	
	
	
	
	
	public String getPicPath() {
		return Properties.ALBUM_PICS_DIR.concat(pic);
	}
	
	public String getReleaseDateString() {
		return String.valueOf(release_date.get(Calendar.YEAR));
	}
	
	
	
	@Override
	public String toString() {
		return id + " " + artist_id + " " + title + " " + pic + " " + release_date + " " + price;
	}
	
	
	
	private Long id;
	private Long artist_id;
	private String title;
	private String pic;
	private Calendar release_date;
	private double price;
	
	private Artists artist;
	private Set tracks;
	
}
