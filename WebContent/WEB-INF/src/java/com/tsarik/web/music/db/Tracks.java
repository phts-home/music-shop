package com.tsarik.web.music.db;


/**
 * 
 * 
 * @author Phil Tsarik
 *
 */
public class Tracks {
	
	public Tracks() {
	}
	
	public Tracks(Long id, Long albumId, String title, int pos, int length, double price, String lyrics) {
		this.id = id;
		this.album_id = albumId;
		this.title = title;
		this.pos = pos;
		this.length = length;
		this.price = price;
		this.lyrics = lyrics;
	}
	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getAlbum_id() {
		return album_id;
	}
	
	public void setAlbum_id(Long albumId) {
		album_id = albumId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public int getPos() {
		return pos;
	}
	
	public void setPos(int pos) {
		this.pos = pos;
	}
	
	public int getLength() {
		return length;
	}
	
	public void setLength(int length) {
		this.length = length;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public String getLyrics() {
		return lyrics;
	}
	
	public void setLyrics(String lyrics) {
		this.lyrics = lyrics;
	}
	
	
	
	
	public Albums getAlbum() {
		return album;
	}
	
	public void setAlbum(Albums album) {
		this.album = album;
	}
	
	
	
	public String getFormattedLength() {
		int h = 0;
		int m = 0;
		int s = length;
		
		while (s >= 60) {
			m++;
			if (m >= 60) {
				h++;
				m -= 60;
			}
			s -= 60;
		}
		
		String res = m + ":";
		if (s < 10) {
			res = res.concat("0");
		}
		res = res.concat(String.valueOf(s));
		if (h != 0) {
			if (m < 10) {
				res = "0".concat(res);
			}
			res = String.valueOf(s).concat(res);
		}
		
		return res;
	}

	
	
	
	@Override
	public String toString() {
		return id + " " + album_id + " " + title + " " + pos + " " + length + " " + price;
	}
	
	
	private Long id;
	private Long album_id;
	private String title;
	private int pos;
	private int length;
	private double price;
	private String lyrics;
	
	
	private Albums album;
	
	
}
