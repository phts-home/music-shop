package com.tsarik.web.music.db;


/**
 * 
 * 
 * @author Phil Tsarik
 *
 */
public class Items {
	
	//public static final String TYPE_ALBUM = "album";
	//public static final String TYPE_TRACK = "track";
	
	public Items() {
		// TODO Auto-generated constructor stub
	}
	
	public Items(Long user_id, Long item_id/*, String item_type*/) {
		this.user_id = user_id;
		this.item_id = item_id;
		//this.item_type = item_type;
	}
	
	public Long getCartitem_id() {
		return cartitem_id;
	}
	
	public void setCartitem_id(Long cartitem_id) {
		this.cartitem_id = cartitem_id;
	}
	
	public Long getUser_id() {
		return user_id;
	}
	
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	
	public Long getItem_id() {
		return item_id;
	}
	
	public void setItem_id(Long item_id) {
		this.item_id = item_id;
	}
	
	/*public String getItem_type() {
		return item_type;
	}
	
	public void setItem_type(String item_type) {
		this.item_type = item_type;
	}*/
	
	protected Long cartitem_id;
	protected Long user_id;
	protected Long item_id;
	//protected String item_type;
	
}
