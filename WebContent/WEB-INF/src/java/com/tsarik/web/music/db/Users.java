package com.tsarik.web.music.db;


/**
 * 
 * 
 * @author Phil Tsarik
 *
 */
public class Users {
	
	public Users() {
		// TODO Auto-generated constructor stub
	}
	
	public Users(String name, String password, String email, Double cash) {
		this.name = name;
		this.password = password;
		this.email = email;
		this.cash = cash;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Double getCash() {
		return cash;
	}
	
	public void setCash(Double cash) {
		this.cash = cash;
	}
	
	
	private Long id;
	private String name;
	private String password;
	private String email;
	private Double cash;
	
}
