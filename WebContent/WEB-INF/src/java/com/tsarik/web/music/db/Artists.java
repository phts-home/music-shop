package com.tsarik.web.music.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.tsarik.web.music.Properties;



/**
 * 
 * 
 * @author Phil Tsarik
 *
 */
public class Artists {
	
	public Artists() {
	}
	
	public Artists(Long id, String name, String info, String pic, String category, Set<Albums> albums) {
		this.id = id;
		this.name = name;
		this.info = info;
		this.pic = pic;
		this.category = category;
		this.albums = albums;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getInfo() {
		return info;
	}
	
	public void setInfo(String info) {
		this.info = info;
	}
	
	public String getPic() {
		return pic;
	}
	
	public void setPic(String pic) {
		this.pic = pic;
	}
	
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public Set<Albums> getAlbums() {
		return albums;
	}
	
	public void setAlbums(Set albums) {
		this.albums = albums;
	}
	
	
	
	
	
	
	public String getPicPath() {
		return Properties.ARTIST_PICS_DIR.concat(pic);
	}
	
	
	
	
	private Long id;
	private String name;
	private String info;
	private String pic;
	private String category;
	private Set<Albums> albums;
	
}
