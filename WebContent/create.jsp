<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="<s:property value="cssFile"/>" type="text/css" rel="stylesheet">
<title>Login - <s:property value="title"/></title>
</head>

<body>
<div class="header"><s:include value="common/header.jsp"/></div>
<div class="menu"><s:include value="common/menu.jsp"/></div>
<div class="letters"><s:include value="common/letters.jsp"/></div>
<div class="caption">Create new account</div>
<div class="content">

<s:form action="Create">
	<s:textfield label="Username" name="username"/>
	<s:password label="Password" name="password"/>
	<s:textfield label="E-Mail" name="email"/>
	<s:submit key="Submit"></s:submit>
</s:form>

</div>
<div class="footer"><s:property value="copyright"/></div>
</body>
</html>