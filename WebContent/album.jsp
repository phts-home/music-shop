<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="<s:property value="cssFile"/>" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<s:property value="jsFile"/>"></script>
<title><s:property value="artist.name" /> - <s:property value="album.title" /> (<s:property value="album.releaseDateString" />) - <s:property value="title"/></title>
</head>

<body>
<div class="header"><s:include value="common/header.jsp"/></div>
<div class="menu"><s:include value="common/menu.jsp"/></div>
<div class="letters"><s:include value="common/letters.jsp"/></div>
<div class="caption"><s:property value="artist.name" /> - <s:property value="album.title" /> (<s:property value="album.releaseDateString" />)</div>
<div class="content">


<div class="album_artist_name">
	<s:url id="url" action="Artist"><s:param name="artist_id" value="artist.id"/></s:url><s:a href="%{url}"><s:property value="artist.name" /></s:a>
</div>

<div class="album_title">
	<s:property value="album.title" />
</div>
<div class="album_pic">
	<img class="album_pic_img" src="<s:property value="album.picPath" />">
</div>
<div class="album_releaseDate">
	<s:property value="album.releaseDateString" />
</div>
<div class="album_buy">
	<s:url id="buyAlbumUrl" action="AddAlbumsToCart"><s:param name="id" value="album.id"/></s:url><s:a href="%{buyAlbumUrl}"><s:text name="cart.buyWholeAlbum"/></s:a>
	(<s:property value="album.price" /><s:property value="currency"/>)
</div>
<div class="album_tracklist">
	<%--s:if test="album.tracks != null && album.tracks.size != 0"--%>
	<s:if test="album.tracks != null && album.tracks.size != 0">
		<s:set name="trackCount" value="%{album.tracks.size}" />
		<s:set name="albumPrice" value="%{album.price}" />
		<div class="album_check_all"><s:checkbox value="false" name="check_all" onclick="checkAll('check_all', 'ch_buy_tr'); sum(%{trackCount}, %{albumPrice});"/></div>
		<s:iterator value="album.tracks" status="st">
		
			<s:if test="#st.index % 2 == 0">
				<div class="album_tracklist_track_1">
			</s:if>
			<s:else>
				<div class="album_tracklist_track_2">
			</s:else>
				<s:checkbox cssClass="ch_buy_tr" value="false" name="%{id}_%{price}" onclick="sum(%{trackCount}, %{albumPrice})"/>
				<s:if test="lyrics != null && lyrics != '' ">
					<s:url id="url" action="Lyrics"><s:param name="track_id" value="id"/></s:url><s:a href="%{url}"><s:property value="pos"/> - <s:property value="title"/></s:a>
				</s:if>
				<s:else>
					<s:property value="pos"/> - <s:property value="title"/>
				</s:else>
					
				(<s:property value="formattedLength"/>)
				
				<s:url id="url" action="AddTracksToCart"><s:param name="id" value="id"/></s:url><s:a href="%{url}"><s:text name="cart.buyTrack"/> <s:property value="price"/><s:property value="currency"/></s:a>
			</div>
		
		</s:iterator>
		<div class="selected_price"><span id="selected_price">0</span><s:property value="currency"/></div>
		<div class="buy_selected_tracks_divbutton"><s:a href="javascript:buySelectedTracks(%{album.tracks.size}, '%{buyAlbumUrl}');"><s:text name="cart.buySelectedTracks"/></s:a></div>
	</s:if>
	<s:else>
		<s:text name="album.noTracksMessage" />
	</s:else>
</div>


</div>
<div class="footer"><s:property value="copyright"/></div>
</body>
</html>