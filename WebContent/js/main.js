
/**
 * Checks or unchecks all checkboxes with targetClassName class name 
 * depending on the checkbox with selfId id value
 */
function checkAll(selfId, targetClassName) {
	var self = document.getElementById(selfId);
	var checks = document.getElementsByTagName('input');
	
	for (var i = 0; i < checks.length; i++) {
		if ( (checks[i].type == 'checkbox') && (checks[i].className == targetClassName) ) {
			checks[i].checked = self.checked;
		}
	}
}

/**
 * Calculates total price of the selected tracks or albums.
 * If selected track count is equal albumTrackCount (all tracks were selected) then Total Price will be albumPrice.
 * Checkboxes' classes: 'ch_buy_alb' (for album) or 'ch_buy_tr' (for track).
 * Checkbox's "name" field must comprise Item ID and Price with pattern: "itemId_itemPrice".
 * Output element's id: 'selected_price'.
 */
function sum(albumTrackCount, albumPrice) {
	var checks = document.getElementsByTagName('input');
	var out = document.getElementById('selected_price');
	var price = 0;
	var count = 0;
	var cl = '';
	
	for (var i = 0; i < checks.length; i++) {
		if ( (checks[i].type == 'checkbox') && ( (checks[i].className == 'ch_buy_alb') || (checks[i].className == 'ch_buy_tr') ) && (checks[i].checked)) {
			cl = checks[i].className;
			count++;
			var p = checks[i].name.search('_');
			if (p == -1) {
				continue;
			}
			var pr = checks[i].name.substring(p+1);
			price += pr * 1.0;
		}
	}
	
	if (cl == 'ch_buy_tr') {
		if (count == albumTrackCount) {
			price = albumPrice;
		}
	}
	
	out.innerHTML = price;
}

/**
 * Executes AddTracksToCart action with albums ids as parameters OR buyAlbumUrl action if all tracks were selected.
 * Checkbox's class: 'ch_buy_tr'.
 * Checkbox's "name" field must comprise Item ID and Price with pattern: "itemId_itemPrice".
 * Output parameters looks like ?id=1,2,3,4
 * 
 * @param albumTrackCount Track count in the current album
 * @param buyAlbumUrl Action URL of 'Buy Album' Action of current album
 */
function buySelectedTracks(albumTrackCount, buyAlbumUrl) {
	var checks = document.getElementsByTagName('input');
	var count = 0;
	
	var params = '?id=';
	var params1 = '';
	
	for (var i = 0; i < checks.length; i++) {
		if ( (checks[i].type == 'checkbox') && (checks[i].name != 'check_all') && (checks[i].className == 'ch_buy_tr') && (checks[i].checked)) {
			count++;
			
			var p = checks[i].name.search('_');
			if (p == -1) {
				continue;
			}
			var id = checks[i].name.substring(0, p);
			
			params1 += id+',';
		}
	}
	if (params1 == '') {
		return;
	}
	
	if (count == albumTrackCount) {
		window.location = buyAlbumUrl;
		return;
	}
	
	// remove last "," in param string
	params += params1.substring(0, params1.length-1);
	
	window.location = '/Music/AddTracksToCart'+params;
}

/**
 * Executes AddAlbumsToCart action with albums ids as parameters.
 * Checkbox's class: 'ch_buy_alb'.
 * Checkbox's "name" field must comprise Item ID and Price with pattern: "itemId_itemPrice".
 * Output parameters looks like ?id=1,2,3,4
 */
function buySelectedAlbums() {
	var checks = document.getElementsByTagName('input');
	
	var params = '?id=';
	var params1 = '';
	
	for (var i = 0; i < checks.length; i++) {
		if ( (checks[i].type == 'checkbox') && (checks[i].name != 'check_all') && (checks[i].className == 'ch_buy_alb') && (checks[i].checked)) {
			var p = checks[i].name.search('_');
			if (p == -1) {
				continue;
			}
			var id = checks[i].name.substring(0, p);
			
			params1 += id+',';
		}
	}
	if (params1 == '') {
		return;
	}
	// remove last "," in param string
	params += params1.substring(0, params1.length-1);
	
	window.location = '/Music/AddAlbumsToCart'+params;
}

/**
 * Removes selected items from the cart. 
 * Checkboxes' classes: 'ch_remove_alb' (for album) or 'ch_remove_tr' (for track).
 * Item ID holds in Checkbox's "name" field.
 * Output parameters looks like ?cartitem=1,2,3,4
 */
function removeSelected() {
	var checks = document.getElementsByTagName('input');
	
	var params = '?cartitem_id=';
	var params1 = '';
	
	for (var i = 0; i < checks.length; i++) {
		if ( (checks[i].type == 'checkbox') && ( (checks[i].className == 'ch_remove_alb') || (checks[i].className == 'ch_remove_tr') ) && (checks[i].checked)) {
			params1 += checks[i].name+',';
		}
	}
	if (params1 == '') {
		return;
	}
	// remove last "," in param string
	params += params1.substring(0, params1.length-1);
	
	window.location = '/Music/RemoveFromCart'+params;
}

