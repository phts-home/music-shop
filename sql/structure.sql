-- phpMyAdmin SQL Dump
-- version 3.2.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 28, 2010 at 09:02 PM
-- Server version: 5.1.40
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `Music`
--

-- --------------------------------------------------------

--
-- Table structure for table `Albums`
--

CREATE TABLE IF NOT EXISTS `Albums` (
  `album_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `artist_id` bigint(20) NOT NULL,
  `title` varchar(127) NOT NULL,
  `pic` varchar(127) DEFAULT NULL,
  `release_date` date NOT NULL,
  `price` double NOT NULL DEFAULT '10',
  PRIMARY KEY (`album_id`),
  KEY `FK750B3C04291309C1` (`artist_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `Albums`
--

INSERT INTO `Albums` (`album_id`, `artist_id`, `title`, `pic`, `release_date`, `price`) VALUES
(1, 1, 'Meddle', 'album_meddle.jpg', '1971-01-01', 10),
(2, 1, 'Wish You Were Here', 'album_wish_you_were_here.jpg', '1975-01-01', 10),
(3, 1, 'The Dark Side Of The Moon', 'album_the_dark_side_of_the_moon.jpg', '1973-01-01', 10);

-- --------------------------------------------------------

--
-- Table structure for table `Artists`
--

CREATE TABLE IF NOT EXISTS `Artists` (
  `artist_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL,
  `info` varchar(1023) NOT NULL,
  `pic` varchar(127) DEFAULT NULL,
  `category` varchar(10) NOT NULL,
  PRIMARY KEY (`artist_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Artists`
--

INSERT INTO `Artists` (`artist_id`, `name`, `info`, `pic`, `category`) VALUES
(1, 'Pink Floyd', 'Pink Floyd were an English rock  band who earned recognition for their psychedelic music in the late 1960s, and as they evolved in the 1970s, for their progressive rock music. Pink Floyd''s work is marked by the use of philosophical lyrics, sonic experimentation, innovative album cover art, and elaborate live shows. One of rock music''s most critically acclaimed and commercially successful acts, the group have sold over 200 million albums worldwide, including 74.5 million certified units in the United States, making them one of the best-selling music artists  of all time.', 'band_pink_floyd.jpg', 'P');

-- --------------------------------------------------------

--
-- Table structure for table `Items`
--

CREATE TABLE IF NOT EXISTS `Items` (
  `cartitem_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `item_type` varchar(10) NOT NULL,
  PRIMARY KEY (`cartitem_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `Items`
--

INSERT INTO `Items` (`cartitem_id`, `user_id`, `item_id`, `item_type`) VALUES
(37, 1, 3, 'album');

-- --------------------------------------------------------

--
-- Table structure for table `Tracks`
--

CREATE TABLE IF NOT EXISTS `Tracks` (
  `track_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `album_id` bigint(20) NOT NULL,
  `title` varchar(127) NOT NULL,
  `pos` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `price` double NOT NULL DEFAULT '2',
  `lyrics` text DEFAULT NULL,
  PRIMARY KEY (`track_id`),
  KEY `FK95CB24A8A58015ED` (`album_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `Tracks`
--

INSERT INTO `Tracks` (`track_id`, `album_id`, `title`, `pos`, `length`, `price`, `lyrics`) VALUES
(1, 1, 'Fearless', 3, 369, 2, NULL),
(2, 1, 'Echoes', 6, 1411, 3, NULL),
(3, 2, 'Welcome To The Machine', 2, 446, 2, NULL),
(4, 2, 'Shine On You Crazy Diamond (Part Two)', 5, 742, 3, NULL),
(5, 1, 'One Of These Days', 1, 358, 2, NULL),
(6, 1, 'San Tropez', 4, 224, 2, NULL),
(7, 2, 'Have A Cigar', 3, 308, 2, NULL),
(8, 2, 'Shine On You Crazy Diamond (Part One)', 1, 810, 3, NULL),
(9, 2, 'Wish You Were Here', 4, 340, 2, NULL),
(10, 1, 'A Pillow Of Winds', 2, 311, 2, NULL),
(11, 1, 'Seamus', 5, 136, 2, NULL),
(12, 3, '(A) Speak To Me (B) Breathe', 1, 238, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `cash` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`user_id`, `name`, `password`, `email`, `cash`) VALUES
(1, 'phil', '123', 'mail@mail.com', 952);
